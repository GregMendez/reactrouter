import { useState } from 'react'
import logo from './logo.svg'
import './App.css'
import { Routes, Route, BrowserRouter } from "react-router-dom"
import Homepage from "./pages/Homepage"
import NoPage from "./pages/NoPage"
import Article from "./pages/Article"
import Blog from "./pages/Blog"

function App() {
  

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/blog" element={<Blog />} />
        <Route path="/blog/article" element={<Article />} />
        <Route path="*" element={<NoPage />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
